src/controller/ControllerImpl.java:142:	This final field could be made static
src/controller/ControllerImpl.java:143:	This final field could be made static
src/model/LotBuilder.java:12:	Avoid using redundant field initializer for 'nextId'
src/model/LotBuilder.java:14:	Perhaps 'id' could be replaced by a local variable.
src/model/LotBuilder.java:15:	Field name has the same name as a method
src/model/LotBuilder.java:16:	Field expirationDate has the same name as a method
src/model/LotBuilder.java:17:	Field checkInDate has the same name as a method
src/model/LotBuilder.java:18:	Avoid using redundant field initializer for 'initialQuantity'
src/model/LotBuilder.java:19:	Avoid using redundant field initializer for 'pricePerSingleItem'
src/model/LotBuilder.java:19:	Field pricePerSingleItem has the same name as a method
src/model/LotBuilder.java:101:	These nested if statements could be combined
src/model/Warehouse.java:25:	Private field 'res' could be made final; it is only initialized in the declaration or constructor.
src/model/discountstrategies/DiscountStrategyFactoryImpl.java:8:	This final field could be made static
src/model/discountstrategies/DiscountStrategyFactoryImpl.java:9:	This final field could be made static
src/model/discountstrategies/ExpiresWithinNDays.java:16:	Private field 'numberOfDays' could be made final; it is only initialized in the declaration or constructor.
src/model/discountstrategies/ExpiresWithinNDays.java:17:	Private field 'discountAmount' could be made final; it is only initialized in the declaration or constructor.
src/model/discountstrategies/ExpiresWithinNDays.java:31:	Local variable 'result' could be declared final
src/model/discountstrategies/OverFiftyDiscount.java:17:	This final field could be made static
src/test/controller/BasicControllerTest.java:25:	Use explicit scoping instead of the default package private level
src/test/controller/BasicControllerTest.java:26:	Use explicit scoping instead of the default package private level
src/test/controller/BasicControllerTest.java:29:	Method names should not contain underscores
src/test/controller/BasicControllerTest.java:29:	Method names should not start with capital letters
src/test/controller/BasicControllerTest.java:33:	Local variable 'controller' could be declared final
src/test/controller/BasicControllerTest.java:53:	Method names should not contain underscores
src/test/controller/BasicControllerTest.java:53:	Method names should not start with capital letters
src/test/controller/BasicControllerTest.java:58:	Local variable 'controller' could be declared final
src/test/controller/BasicControllerTest.java:71:	Method names should not contain underscores
src/test/controller/BasicControllerTest.java:71:	Method names should not start with capital letters
src/test/controller/BasicControllerTest.java:75:	Local variable 'controller' could be declared final
src/test/controller/BasicControllerTest.java:88:	Method names should not contain underscores
src/test/controller/BasicControllerTest.java:88:	Method names should not start with capital letters
src/test/controller/BasicControllerTest.java:92:	Local variable 'controller' could be declared final
src/test/controller/BasicControllerTest.java:97:	Local variable 'l' could be declared final
src/test/controller/BasicControllerTest.java:108:	Method names should not contain underscores
src/test/controller/BasicControllerTest.java:108:	Method names should not start with capital letters
src/test/controller/BasicControllerTest.java:110:	Local variable 'lot' could be declared final
src/test/controller/BasicControllerTest.java:118:	Local variable 'controller' could be declared final
src/test/controller/BasicControllerTest.java:120:	Local variable 'l' could be declared final
src/test/model/BasicExceptionTest.java:17:	Method names should not contain underscores
src/test/model/BasicExceptionTest.java:17:	Method names should not start with capital letters
src/test/model/BasicExceptionTest.java:26:	Method names should not contain underscores
src/test/model/BasicExceptionTest.java:26:	Method names should not start with capital letters
src/test/model/BasicExceptionTest.java:35:	Method names should not contain underscores
src/test/model/BasicExceptionTest.java:35:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:32:	Avoid using redundant field initializer for 'l'
src/test/model/BasicModelTest.java:32:	Use explicit scoping instead of the default package private level
src/test/model/BasicModelTest.java:33:	Avoid using redundant field initializer for 'p'
src/test/model/BasicModelTest.java:33:	Use explicit scoping instead of the default package private level
src/test/model/BasicModelTest.java:34:	Avoid using redundant field initializer for 'o'
src/test/model/BasicModelTest.java:34:	Use explicit scoping instead of the default package private level
src/test/model/BasicModelTest.java:35:	Avoid using redundant field initializer for 'c'
src/test/model/BasicModelTest.java:35:	Use explicit scoping instead of the default package private level
src/test/model/BasicModelTest.java:36:	Avoid using redundant field initializer for 's'
src/test/model/BasicModelTest.java:36:	Use explicit scoping instead of the default package private level
src/test/model/BasicModelTest.java:37:	Avoid using redundant field initializer for 'b'
src/test/model/BasicModelTest.java:37:	Use explicit scoping instead of the default package private level
src/test/model/BasicModelTest.java:43:	Method names should not contain underscores
src/test/model/BasicModelTest.java:43:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:62:	Method names should not contain underscores
src/test/model/BasicModelTest.java:62:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:78:	Method names should not contain underscores
src/test/model/BasicModelTest.java:78:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:92:	Method names should not contain underscores
src/test/model/BasicModelTest.java:92:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:108:	Method names should not contain underscores
src/test/model/BasicModelTest.java:108:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:113:	Local variable 'm' could be declared final
src/test/model/BasicModelTest.java:118:	Local variable 'factory' could be declared final
src/test/model/BasicModelTest.java:134:	Method names should not contain underscores
src/test/model/BasicModelTest.java:134:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:149:	Local variable 'factory' could be declared final
src/test/model/BasicModelTest.java:162:	Method names should not contain underscores
src/test/model/BasicModelTest.java:162:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:167:	Local variable 'm' could be declared final
src/test/model/BasicModelTest.java:172:	Local variable 'factory' could be declared final
src/test/model/BasicModelTest.java:174:	Local variable 'x' could be declared final
src/test/model/BasicModelTest.java:177:	Local variable 'x2' could be declared final
src/test/model/BasicModelTest.java:186:	Method names should not contain underscores
src/test/model/BasicModelTest.java:186:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:191:	Local variable 'm' could be declared final
src/test/model/BasicModelTest.java:196:	Local variable 'factory' could be declared final
src/test/model/BasicModelTest.java:198:	Local variable 'x' could be declared final
src/test/model/BasicModelTest.java:201:	Local variable 'x2' could be declared final
src/test/model/BasicModelTest.java:209:	Method names should not contain underscores
src/test/model/BasicModelTest.java:209:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:215:	Local variable 'm' could be declared final
src/test/model/BasicModelTest.java:221:	Local variable 'factory' could be declared final
src/test/model/BasicModelTest.java:234:	Method names should not contain underscores
src/test/model/BasicModelTest.java:234:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:240:	Local variable 'm' could be declared final
src/test/model/BasicModelTest.java:246:	Local variable 'factory' could be declared final
src/test/model/BasicModelTest.java:267:	Method names should not contain underscores
src/test/model/BasicModelTest.java:267:	Method names should not start with capital letters
src/test/model/BasicModelTest.java:273:	Local variable 'm' could be declared final
src/test/model/BasicModelTest.java:282:	Use assertSame(x, y) instead of assertTrue(x==y), or assertNotSame(x,y) vs assertFalse(x==y)
src/test/model/BasicModelTest.java:284:	Use assertSame(x, y) instead of assertTrue(x==y), or assertNotSame(x,y) vs assertFalse(x==y)
src/test/model/SerializationTest.java:22:	Avoid using redundant field initializer for 'l'
src/test/model/SerializationTest.java:22:	Use explicit scoping instead of the default package private level
src/test/model/SerializationTest.java:23:	Avoid using redundant field initializer for 'p'
src/test/model/SerializationTest.java:23:	Use explicit scoping instead of the default package private level
src/test/model/SerializationTest.java:30:	Local variable 'm' could be declared final
src/test/model/SerializationTest.java:31:	Local variable 'c' could be declared final
src/test/model/SerializationTest.java:46:	Local variable 'm2' could be declared final
src/test/model/SerializationTest.java:47:	Local variable 'c2' could be declared final
src/test/view/ViewTesting.java:24:	Method names should not contain underscores
src/test/view/ViewTesting.java:24:	Method names should not start with capital letters
src/view/ViewImpl.java:35:	Perhaps 'getLots' could be replaced by a local variable.
src/view/ViewImpl.java:36:	Perhaps 'getDiscountable' could be replaced by a local variable.
src/view/ViewImpl.java:37:	Perhaps 'operations' could be replaced by a local variable.
view/frames/BasicOperationsOnLots.java:43:36: Redundant 'final' modifier. [RedundantModifier]
view/frames/BasicOperationsOnLots.java:43:65: Redundant 'final' modifier. [RedundantModifier]
view/frames/BasicOperationsOnLots.java:43:83: Redundant 'final' modifier. [RedundantModifier]
